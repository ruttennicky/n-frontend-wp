<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
	$terms = get_terms(array('taxonomy' => 'product_cat','exclude' => 15))
?>
<div class="row p-top-40">
	<div class="col-12">
		<ul class="list-inline" id="grid-filter">
				<li class="list-inline-item"><span class="f-s-09 f-bold"><?php _e('lbl_showitems','knoopsschat'); ?></span></li>
				<li class="list-inline-item"><a href="#" data-filter="*" class="badge badge-pill active"><?php _e('lbl_allitems','knoopsschat'); ?></a></li>
<?php 	foreach ($terms as $term):		?>
				<li class="list-inline-item"><a href="#" data-filter=".<?php echo $term->slug; ?>" class="badge badge-pill f-white"><?php echo $term->name; ?></a></li>
<?php 	endforeach;			?>
		</ul>
	</div>
</div>
<div class="row grid">
