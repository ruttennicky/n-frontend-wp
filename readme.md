# N digital WP template boilerplate

This git repository is the base for all WordPress-powered projects of N digital studio.
Follow this step by step tutorial to install a clean version of Wordpress for a next project.

### Step 1
Using your terminal, go to the folder where WordPress has to be installed and download WP-CLI:
```
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
```

### Step 2
Create a wp-install file and paste these instruction into the file:
```
echo "What is the name of the database?"
read dbname
echo "What is the username of the database user?"
read dbuser
echo "What is the password of the database user?"
read dbpass
echo "What is the hostname of the database server?"
read dbhost
echo "What is the hostame of the website?"
read sitehost
echo "What is the title of the website?"
read sitetitle
echo "What is the username of the website administrator?"
read adminuser
echo "What is the password of the website administrator?"
read adminpass
echo "What is the email address of the website administrator?"
read adminemail

php wp-cli.phar config create --dbname="$dbname" --dbuser="$dbuser" --dbpass="$dbpass" --dbhost="$dbhost" --locale=nl_NL
php wp-cli.phar db create
php wp-cli.phar core install --url="$sitehost" --title="$sitetitle"  --admin_user="$adminuser" --admin_password="$adminpass" --admin_email="$adminemail"
php wp-cli.phar theme activate nfrontend
php wp-cli.phar plugin install woocommerce wordpress-seo wp-html-mail ninja-forms mailchimp-for-wp regenerate-thumbnails postmark-approved-wordpress-plugin webp-converter-for-media w3-total-cache
php wp-cli.phar core update
php wp-cli.phar core update-db
php wp-cli.phar plugin update --all
php wp-cli.phar core language update
```

### Step 3
Execute the following command:
```
sh wp-install.sh
```

### Step 4 (optional)
Setup a virtual host if you're working on a local machine

### Step 5
Navigate to wp-content > themes and clone the boilerplate setup:
```
git clone git@gitlab.com:ruttennicky/n-frontend-wp.git .
```

### Step 6
Duplicate the customer_base folder and rename it the name of the project

### Step 7
Using your command line interface, execute in the new theme folder:
```
yarn install
```
> Note: for this you'll need Node JS and Yarn (and Homebrew possibly)

### Step 8
Use Gulp to compile CSS, JS and optimize images:
```
gulp serve
```

### Step 9
Sit back, enjoy and smile, you've just installed a brand new -up to date- copy of WordPress.
You can now start developing. All css and javascript changes will compile automatically from the correct folders.

### Step 10
If the CSS file of your new theme doesn't get loaded, activate another theme and reactivate your own theme back again.
