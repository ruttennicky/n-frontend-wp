<?php
	function load_js_css() {
		if(!is_admin()) {
				wp_deregister_script('jquery');
				wp_enqueue_script('jquery','//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js',array(),false,true);
				wp_enqueue_script('jquery-mmenu-pollyfill','//cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.20/mmenu.polyfills.min.js',array('jquery'),false,true);
	      wp_enqueue_script('jquery-mmenu','//cdnjs.cloudflare.com/ajax/libs/mmenu-js/8.5.20/mmenu.min.js',array('jquery'),false,true);
	      wp_enqueue_script('jquery-fancybox','//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.0/jquery.fancybox.min.js',array('jquery'),false,true);
				wp_enqueue_script('jquery-slick','//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js',array('jquery'),false,true);
				wp_enqueue_script('jquery-css3animateit', '//cdnjs.cloudflare.com/ajax/libs/css3-animate-it/1.0.3/js/css3-animate-it.min.js',array('jquery'),false,true);
				wp_enqueue_script('pingdom','//rum-static.pingdom.net/pa-5c4cbec69a3f8300160004db.js',array('jquery'),false,true);
				if (function_exists('get_field')) {
					if (get_field('google_api_key','option')) {
						wp_enqueue_script('googlemaps','//maps.googleapis.com/maps/api/js?key='. get_field('google_api_key','option'),array('jquery'),false,true);
					}
					if (get_field('blueconic_id','option')) {
						wp_enqueue_script('blueconic','//cdn.blueconic.net/'. get_field('blueconic_id','option') .'.js',array('jquery'),false,true);
					}
					if (get_field('font_awesome_id','option')) {
						wp_enqueue_script('font-awesome','https://kit.fontawesome.com/'. get_field('font_awesome_id','option') .'.js',array(),false,true);
					}
				}
				wp_enqueue_script('nfrontend', get_template_directory_uri() . '/js/nfrontend.js',array('jquery'),false,true);

				wp_dequeue_style('mailchimp-for-wp-checkbox');
				wp_dequeue_style('boxes');
				wp_dequeue_style('jquery-qtip');
				wp_dequeue_style('jquery-rating');

				wp_enqueue_style('jquery-fancybox','//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.0/jquery.fancybox.min.css',false,false,'all');
				wp_enqueue_style('jquery-css3animateit','//cdnjs.cloudflare.com/ajax/libs/css3-animate-it/1.0.3/css/animations.min.css',false,false,'all');
				wp_enqueue_style('nfrontend',get_stylesheet_uri(),false,false,'all');
    }
	}

	function load_js_tags($tag,$handle,$src) {
		$async_scripts = array();
		$defer_scripts = array();
		$crossorgin_scripts = array('font-awesome');

		if (in_array($handle, $defer_scripts)) {
				return '<script src="' . $src . '" defer="defer"></script>' . "\n";
		}
		if (in_array($handle, $async_scripts)) {
				return '<script src="' . $src . '" async="async"></script>' . "\n";
		}
		if (in_array($handle, $crossorgin_scripts)) {
				return '<script src="' . $src . '" crossorigin="anonymous"></script>' . "\n";
		}
		return $tag;
	}

	function load_widgets() {
    register_sidebar( array(
        'name'          => 'Sidebar',
        'id'            => 'sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 id="sidebar">',
        'after_title'   => '</h3>',
    ) );
	}

  function load_metas() {
      echo '
          <meta http-equiv="X-UA-Compatible" content="IE=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		      <link rel="apple-touch-icon" sizes="180x180" href="'. get_stylesheet_directory_uri() .'/images/favicons/apple-touch-icon.png">
		      <link rel="icon" type="image/png" href="'. get_stylesheet_directory_uri() .'/images/favicons/favicon-32x32.png" sizes="32x32">
		      <link rel="icon" type="image/png" href="'. get_stylesheet_directory_uri() .'/images/favicons/favicon-16x16.png" sizes="16x16">
		      <link rel="mask-icon" href="'. get_stylesheet_directory_uri() .'/images/favicons/safari-pinned-tab.svg">
		      <meta name="theme-color" content="#ffffff">
      ';
  }

	function load_hj() {
		if (function_exists('get_field')) {
			if (get_field('hotjar_id','option')) {
			echo "<script>
		    		(function(h,o,t,j,a,r){
		        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
		        h._hjSettings={hjid:". get_field('hotjar_id','option') .",hjsv:6};
		        a=o.getElementsByTagName('head')[0];
		        r=o.createElement('script');r.async=1;
		        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
		        a.appendChild(r);
		    		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
						</script>";
			}
		}
	}

  function load_ga() {
		if (function_exists('get_field')) {
			if (get_field('google_analytics_id','option')) {
		    echo "<script async src='//www.googletagmanager.com/gtag/js?id=". get_field('google_analytics_id','option') ."'></script>
							<script>
							  window.dataLayer = window.dataLayer || [];
							  function gtag(){dataLayer.push(arguments);}
							  gtag('js', new Date());
							  gtag('config', '". get_field('google_analytics_id','option') ."',{'anonymize_ip': true });
							</script>";
			}
		}
  }

  function load_gtm() {
		if (function_exists('get_field')) {
			if (get_field('google_tag_manager_id','option')) {
		    echo "<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
							new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
							j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
							'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
							})(window,document,'script','dataLayer','". get_field('google_tag_manager_id','option') ."');</script>";
			}
		}
	}

	function load_fb() {
		if (function_exists('get_field')) {
			if (get_field('facebook_page_id','option')) {
				echo "<div id='fb-root'></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
					js.src = 'https://connect.facebook.net/". get_locale() ."/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>";
			}
		}
	}

	function load_drift() {
		if (function_exists('get_field')) {
			if (get_field('drift_id','option')) {
				echo '
				<script>
				"use strict";

				!function() {
				  var t = window.driftt = window.drift = window.driftt || [];
				  if (!t.init) {
				    if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
				    t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ],
				    t.factory = function(e) {
				      return function() {
				        var n = Array.prototype.slice.call(arguments);
				        return n.unshift(e), t.push(n), t;
				      };
				    }, t.methods.forEach(function(e) {
				      t[e] = t.factory(e);
				    }), t.load = function(t) {
				      var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
				      o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
				      var i = document.getElementsByTagName("script")[0];
				      i.parentNode.insertBefore(o, i);
				    };
				  }
				}();
				drift.SNIPPET_VERSION = "0.3.1";
				drift.load("'. get_field('drift_id','option') .'");
				</script>
				';
			}
		}
	}

	function load_cookiebot() {
		if (function_exists('get_field')) {
			if (get_field('cookiebot_id','option')) {
				echo '
					<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="'. get_field('cookiebot_id','option') .'" data-blockingmode="auto" type="text/javascript"></script>
				';
			}
		}
	}

	function load_cookiebot_dialog() {
		if (function_exists('get_field')) {
			if (get_field('cookiebot_id','option')) {
				echo '
					<script id="CookieDeclaration" src="https://consent.cookiebot.com/'. get_field('cookiebot_id','option') .'/cd.js" type="text/javascript" async></script>
				';
			}
		}
	}

	function load_theme_setup() {
		load_theme_textdomain('nfrontend',get_template_directory() .'/languages');
	}

	function load_theme_blocks() {
	  register_nav_menus(
	    array(
	      'main-menu' 	=> 'Main menu',
	      'footer-menu' => 'Footer menu',
        'mobile-menu' => 'Mobile menu',
        'meta-menu' 	=> 'Meta menu',
        'lang-menu' 	=> 'Language menu'
	    )
	  );
	}

	function load_login_logo() {
    echo '
    <style type="text/css">
    	body.login div#login h1 a {
        	background-image: url('. get_stylesheet_directory_uri() .'/images/favicons/android-chrome-192x192.png);
   		}
		</style>
    ';
	}

	function remove_verions_js_css($src) {
		if (strpos($src,'ver='))
			$src = remove_query_arg('ver',$src);
		return $src;
	}

	function load_acf_settings() {
		if (function_exists('get_field')) {
			if (get_field('google_api_key','option')) {
				acf_update_setting('google_api_key',get_field('google_api_key','option'));
			}
		}
		if (function_exists('acf_add_options_page')) {
			acf_add_options_page(array(
				'page_title'    => 'Website options',
				'menu_title'    => 'Website options',
				'menu_slug'     => 'website-options',
				'capability'    => 'edit_posts',
				'icon_url'			=> 'dashicons-admin-site-alt3',
				'redirect'      => false
			));
		}
		if (function_exists('acf_add_local_field_group')) {
			acf_add_local_field_group(array(
				'key' => 'group_604b527988927',
				'title' => 'General',
				'fields' => array(
					array(
						'key' => 'field_604b52811771a',
						'label' => 'Melding',
						'name' => 'melding',
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'tabs' => 'all',
						'toolbar' => 'basic',
						'media_upload' => 0,
						'delay' => 1,
					),
					array(
						'key' => 'field_604b52921771b',
						'label' => 'Adres',
						'name' => 'adres',
						'type' => 'wysiwyg',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'tabs' => 'all',
						'toolbar' => 'basic',
						'media_upload' => 0,
						'delay' => 1,
					),
					array(
						'key' => 'field_604bc0bf6241c',
						'label' => 'Thema kleur',
						'name' => 'thema_kleur',
						'type' => 'color_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'website-options',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

			acf_add_local_field_group(array(
				'key' => 'group_604b41d951523',
				'title' => 'ID\'s',
				'fields' => array(
					array(
						'key' => 'field_604b41dd9691f',
						'label' => 'Contact page ID',
						'name' => 'contact_page_id',
						'type' => 'post_object',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'post_type' => array(
							0 => 'page',
						),
						'taxonomy' => '',
						'allow_null' => 0,
						'multiple' => 0,
						'return_format' => 'id',
						'ui' => 1,
					),
					array(
						'key' => 'field_604b41f896920',
						'label' => 'Contact form ID',
						'name' => 'contact_form_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_604b42430de09',
						'label' => 'Vacature page ID',
						'name' => 'vacature_page_id',
						'type' => 'post_object',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'post_type' => array(
							0 => 'page',
						),
						'taxonomy' => '',
						'allow_null' => 0,
						'multiple' => 0,
						'return_format' => 'object',
						'ui' => 1,
					),
					array(
						'key' => 'field_604b425e0de0a',
						'label' => 'Blog page ID',
						'name' => 'blog_page_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_604bceb3a8905',
						'label' => 'Mailchimp list ID',
						'name' => 'mailchimp_list_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_61575e332bdaf',
						'label' => 'Thumbnail fallback ID',
						'name' => 'thumbnail_fallback_id',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'id',
						'preview_size' => 'medium',
						'library' => 'all',
						'min_width' => '',
						'min_height' => '',
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'website-options',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

			acf_add_local_field_group(array(
				'key' => 'group_604b4272bf5a9',
				'title' => 'Social media',
				'fields' => array(
					array(
						'key' => 'field_604b427601add',
						'label' => 'Facebook url',
						'name' => 'facebook_url',
						'type' => 'url',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'https://www.facebook.com/',
					),
					array(
						'key' => 'field_604b428e01ade',
						'label' => 'Twitter url',
						'name' => 'twitter_url',
						'type' => 'url',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'https://www.twitter.com/',
					),
					array(
						'key' => 'field_604b42a201adf',
						'label' => 'Instagram url',
						'name' => 'instagram_url',
						'type' => 'url',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'https://www.instagram.com/',
					),
					array(
						'key' => 'field_604b42b801ae0',
						'label' => 'Pinterest url',
						'name' => 'pinterest_url',
						'type' => 'url',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'https://www.pinterest.com/',
					),
					array(
						'key' => 'field_604b42df905ae',
						'label' => 'LinkedIn url',
						'name' => 'linkedin_url',
						'type' => 'url',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'https://www.linkedin.com/',
					),
					array(
						'key' => 'field_604b42f1905af',
						'label' => 'Portfolio url',
						'name' => 'portfolio_url',
						'type' => 'url',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => 'https://',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'website-options',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));

			acf_add_local_field_group(array(
				'key' => 'group_604b410bbf189',
				'title' => 'Software',
				'fields' => array(
					array(
						'key' => 'field_604b412706ce1',
						'label' => 'Google Analytics ID',
						'name' => 'google_analytics_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_604b5209e183d',
						'label' => 'Google Tag Manager ID',
						'name' => 'google_tag_manager_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_604b41622d3e1',
						'label' => 'Google API key',
						'name' => 'google_api_key',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_604b41bd22565',
						'label' => 'Hotjar ID',
						'name' => 'hotjar_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_604b5220e183e',
						'label' => 'Facebook page ID',
						'name' => 'facebook_page_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_604b522de183f',
						'label' => 'Blueconic ID',
						'name' => 'blueconic_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_604b5234e1840',
						'label' => 'Drift ID',
						'name' => 'drift_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_604b5244e1841',
						'label' => 'Cookiebot ID',
						'name' => 'cookiebot_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_604b5250e1842',
						'label' => 'Font awesome ID',
						'name' => 'font_awesome_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'website-options',
						),
					),
				),
				'menu_order' => 0,
				'position' => 'normal',
				'style' => 'default',
				'label_placement' => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen' => '',
				'active' => true,
				'description' => '',
			));
		}
	}

	function load_upload_mimes($existing_mimes) {
		$existing_mimes['webp'] = 'image/webp';
		return $existing_mimes;
	}

	function load_woocommerce_support() {
	 	add_theme_support('woocommerce');
	}

	function load_credits($client) {
		if ($client)
			return sprintf(__('txt_credits %1$s','nfrontend'),sprintf('<a href="%s" title="%s">%s</a>','https://www.n-digital.be?utm_source='. $client .'&utm_medium=referral',  __('lbl_titlecredits','nfrontend'), __('lbl_credits','nfrontend')));
	}
 
	function ndig_stop_update_emails($send,$type,$core_update,$result) {
		if ( ! empty( $type ) && $type == 'success' ) {
			return false;
		}
		return true;
	}

	add_action('wp_enqueue_scripts','load_js_css');
  add_action('wp_head','load_metas',10);
	add_action('wp_head','load_cookiebot_dialog',20);
	add_action('wp_footer','load_ga',40);
	add_action('wp_footer','load_gtm',50);
	add_action('wp_footer','load_hj',60);
	add_action('wp_footer','load_fb',70);
	add_action('wp_footer','load_drift',80);
	add_action('wp_footer','load_cookiebot',90);

	add_action('init','load_theme_blocks');
	add_action('widgets_init','load_widgets');
	add_action('acf/init','load_acf_settings');
  add_action('login_enqueue_scripts','load_login_logo');
  add_action('after_setup_theme','load_theme_setup');
  add_action('after_setup_theme','load_woocommerce_support');

  add_filter('style_loader_src','remove_verions_js_css',9999);
  add_filter('script_loader_src','remove_verions_js_css',9999);
	add_filter('script_loader_tag','load_js_tags',10,3);
	add_filter('mime_types','load_upload_mimes');
	add_filter('auto_plugin_update_send_email','__return_false');
	add_filter('auto_theme_update_send_email','__return_false');
	add_filter('auto_core_update_send_email','ndig_stop_update_emails',10,4);

  add_theme_support('yoast-seo-breadcrumbs');
	add_theme_support('post-thumbnails');
	add_theme_support('title-tag');

	remove_action('wp_head','wp_generator');
	remove_action('wp_head','print_emoji_detection_script',7);
	remove_action('wp_print_styles','print_emoji_styles');

	remove_action('admin_print_scripts','print_emoji_detection_script');
	remove_action('admin_print_styles','print_emoji_styles');
?>
