     </main>
      <footer class="container-fluid border-light-grey-top p-top-60 p-bottom-60 f-s-09">
        <div class="row">
          <nav class="col-xs-12 text-center">
      <?php
            wp_nav_menu(	array( 	'theme_location' => 'footer-menu',
                            'menu_class' => 'm-0 list-inline',
                            'container' => '',
                            'menu_id' => 'footer-menu'    ) );
      ?>
          </nav>
        </div>
      <?php
        if (function_exists('get_field')):
          if (get_field('adres','option')):
      ?>
          <div class="row p-top-40">
              <div class="col-xs-12 text-center"><?php the_field('adres','option') ?></div>
          </div>
      <?php
          endif;
        endif;
        if (function_exists('get_field')):
      ?>
        <div class="row p-top-40">
          <div class="col-xs-12 text-center f-s-15">
            <ul class="list-inline">
      <?php
            if (get_field('facebook_url','option')):
      ?>
                <li class="list-inline-item">
                  <a href="<?php the_field('facebook_url','option'); ?>" title="<?php _e('lbl_facebooklink','nfrontend'); ?>" class="no-border">
                    <i class="fab fa-facebook-official"></i>
                  </a>
                </li>
      <?php
            endif;
            if (get_field('twitter_url','option')):
      ?>
                <li class="list-inline-item">
                  <a href="<?php the_field('twitter_url','option'); ?>" title="<?php _e('lbl_twitterlink','nfrontend'); ?>" class="no-border">
                    <i class="fab fa-twitter"></i>
                  </a>
                </li>
      <?php
              endif;
              if (get_field('twitter_url','option')):
      ?>
                <li class="list-inline-item">
                  <a href="<?php the_field('instagram_url','option'); ?>" title="<?php _e('lbl_instagramlink','nfrontend'); ?>" class="no-border">
                    <i class="fab fa-instagram"></i>
                  </a>
                </li>
      <?php
              endif;
              if (get_field('linkedin_url','option')):
      ?>
                <li class="list-inline-item">
                  <a href="<?php the_field('linkedin_url','option'); ?>" title="<?php _e('lbl_linkedinlink','nfrontend'); ?>" class="no-border">
                    <i class="fab fa-linkedin"></i>
                  </a>
                </li>
      <?php
              endif;
              if (get_field('pinterest_url','option')):
      ?>
                <li class="list-inline-item">
                  <a href="<?php the_field('pinterest_url','option'); ?>" title="<?php _e('lbl_pinterestlink','nfrontend'); ?>" class="no-border">
                    <i class="fab fa-pinterest"></i>
                  </a>
                </li>
      <?php    endif;    ?>
            </ul>
            <div class="m-top-20"><?php echo load_credits("nfrontendwpbe"); ?></div>
          </div>
        </div>
      <?php  endif;   ?>
      </footer>
    </div>
    <nav id="mobile-menu">
<?php
      wp_nav_menu(array('theme_location' => 'mobile-menu','container' => '','menu_class' => 'm-0'));

?>
    </nav>
<?php
      if (function_exists('get_field')):
        if (get_field('facebook_page_id','option')):
?>
        <div class="fb-customerchat"
          attribution="setup_tool"
          greeting_dialog_display="hide"
          page_id="<?php the_field('facebook_page_id','option'); ?>"
          <?php if (get_field('thema_kleur','option')): ?>
          theme_color="<?php the_field('thema_kleur','option'); ?>"
          <?php endif; ?>
        >
        </div>
<?php
        endif;
      endif;
      wp_footer();
?>
  </body>
</html>
